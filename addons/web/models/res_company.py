# -*- coding: utf-8 -*-
# Part of Flectra. See LICENSE file for full copyright and licensing details

from flectra import api, fields, models


class ResCompany(models.Model):
    _inherit = 'res.company'

    apps_dashboard_background = fields.Binary(
        string='Apps Dashboard Background')
    login_background = fields.Binary(
        string='Login Background')

    @api.multi
    def check_apps_dashboard_background(self, company_id):
        res_company = self.env['res.company'].sudo().search(
            [('id', '=', company_id)], limit=1)
        if res_company.apps_dashboard_background:
            return 1
        else:
            return 0
