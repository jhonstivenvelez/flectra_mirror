flectra.define('web.AppDashboard', function (require) {
    "use strict";

    var core = require('web.core');
    var Widget = require('web.Widget');
    var rpc = require('web.rpc');
    var session = require('web.session');
    var QWeb = core.qweb;

    var AppDashboard = Widget.extend({
        template: 'AppDashboard',
        events: {
            'input input.o_menu_search_input': function (e) {
                this._onUpdate(e.target.value);
                if ($('.o_menu_search_input').val().length > 0) {
                    $('.flectra_app_switcher_scrollable').css('max-width', '768px');
                }
                else {
                    $('.flectra_app_switcher_scrollable').css('max-width', '1170px');
                }
            },
            'click .o_menuitem': '_onMenuitemClick'
        },
        init: function (parent, menu_data) {
            this._super.apply(this, arguments);
            this.menu_data = this._getChildrenList(menu_data);
        },
        start: function () {
            var self = this;
            this.is_search = false;
            this._onUpdate();
            return this._super.apply(this, arguments).then(function () {
                rpc.query({
                    model: 'res.company',
                    method: 'check_apps_dashboard_background',
                    args: [[], session.company_id],
                }).then(function (result) {
                    if (result) {
                        var location = window.location;
                        var background_image_path = location.origin + location.pathname + '/image?model=res.company&field=apps_dashboard_background&id=' + session.company_id;
                        self.$el.css({
                            'background-image': 'url(' + background_image_path + ')',
                            'background-size': 'cover',
                            'padding-left': '15px'
                        });
                    }
                });
            });
        },
        _render: function () {
            $(this.el).find('.flectra_app_switcher_scrollable').html(QWeb.render('AppDashboard.Content', {
                apps: this.apps,
                menus: this.menus
            }));
        },
        _onMenuitemClick: function (e) {
            e.preventDefault();
            var menu_id = $(e.currentTarget).data('menu');
            this._openMenu(_.findWhere(this.menu_data, {id: menu_id}));
        },
        _openMenu: function (menu) {
            this.trigger_up('on_menu_action', {
                menu_id: menu.id,
                action_id: menu.action
            });
        },
        _onUpdate: function (search_str) {
            var self = this;
            this.apps = [];
            this.menus = [];
            if (!search_str) {
                search_str = '';
            }
            _.each(this.menu_data, function (data) {
                if (data.label.toLowerCase().indexOf(search_str) !== -1) {
                    if (data.is_app) {
                        self.apps.push(data);
                    } else {
                        self.menus.push(data);
                    }
                }
            });
            if (!this.is_search || search_str === '') {
                this.menus = [];
                this.is_search = true;
            }
            this._render();
        },
        _getChildrenList: function (app, result, menu_id) {
            result = result || [];
            menu_id = menu_id || false;
            var item = {
                label: (app.parent_id && app.parent_id.length) ? [app.parent_id[1], app.name].join('/').replace('/', ' / ') : app.name,
                id: app.id,
                xmlid: app.xmlid,
                action: app.action ? app.action.split(',')[1] : '',
                is_app: !app.parent_id,
                web_icon: app.web_icon
            };

            if (!app.parent_id) {
                if (app.web_icon_data) {
                    item.web_icon_data = 'data:image/png;base64,' + app.web_icon_data;
                } else if (app.web_icon) {
                    var icon = app.web_icon.split(',');
                    item.web_icon = {
                        foreground: icon[0],
                        background: icon[1],
                        icon_name: icon[2],
                    };
                } else {
                    item.web_icon_data = '/web/static/src/img/icon.png';
                }
            } else {
                item.menu_id = menu_id;
            }

            if (item.action) {
                result.push(item);
            }
            if (app.children && app.children.length) {
                for (var i in app.children) {
                    if (app.children[i].parent_id === false) {
                        menu_id = app.children[i].id;
                    }
                    this._getChildrenList(app.children[i], result, menu_id);
                }
            }
            return result;
        }
    });
    return AppDashboard;
});
