flectra.define('web.BackendThemeMenu', function (require) {
    "use strict";

    var core = require('web.core');
    var config = require('web.config');
    var Widget = require('web.Widget');
    var SystrayMenu = require('web.SystrayMenu');
    var UserMenu = require('web.UserMenu');

    UserMenu.prototype.sequence = 0;
    SystrayMenu.Items.push(UserMenu);

    var QWeb = core.qweb;

    var Menu = Widget.extend({
        template: 'Menu',
        events: {
            'click .o_menu_toggle': function (e) {
                this.trigger_up('_onClickMenuToggle', e);
            },
            'click .o_mobile_menu_toggle': function () {
                this.$el.parent().toggleClass('o_mobile_menu_opened');
            },
            'click a[data-target="#o_navbar_collapse.in"]': function (e) {
                this.$el.parent().toggleClass('o_mobile_menu_opened');
            },
            'click a[data-menu]': '_onMenuitemClick'
        },
        init: function (parent, menu_data) {
            var self = this;
            this._super.apply(this, arguments);
            this.menu_data = menu_data;
            this.all_menu_data = this._getChildrenList(menu_data);
            this.parent = parent;
            this.$menu_sections = {};

            var $menu_sections = $(QWeb.render('Menu.sections', {'menu_data': this.menu_data}));

            $menu_sections.filter('section').each(function () {
                self.$menu_sections[parseInt($(this).attr('class'))] = $(this).children('li');
            });
        },
        start: function () {
            this.systray_menu = new SystrayMenu(this);
            this.systray_menu.attachTo(this.$('.o_menu_systray'));

            core.bus.on('update_menu_id_url', this, this._render);
            core.bus.on("resize", this, _.debounce(this._handleMoreItems));
            return this._super.apply(this, arguments);
        },
        _actionIdToMenuId: function (action_id) {
            var menu_obj = _.findWhere(this.all_menu_data, {
                action: action_id.toString(),
                is_app: false
            });
            return menu_obj ? menu_obj['menu_id'] : undefined;
        },
        _render: function (primary_menu) {
            this.current_primary_menu = primary_menu;
            var menu_obj = _.findWhere(this.menu_data.children, {id: primary_menu});
            if (menu_obj) {
                var app_name = menu_obj.name;
                $('.o_menu_brand').html(app_name);
                $('.o_menu_sections').html(this.$menu_sections[primary_menu]);
                this._handleMoreItems();
            }
        },
        _menuIdToActionId: function (menu_id) {
            var menu_obj = _.findWhere(this.all_menu_data, {
                id: menu_id,
                is_app: true
            });
            return menu_obj ? parseInt(menu_obj['action']) : undefined;
        },
        _getChildrenList: function (app, result, menu_id) {
            result = result || [];
            menu_id = menu_id || false;
            var item = {
                label: (app.parent_id && app.parent_id.length) ? [app.parent_id[1], app.name].join('/').replace('/', ' / ') : app.name,
                id: app.id,
                xmlid: app.xmlid,
                action: app.action ? app.action.split(',')[1] : '',
                is_app: !app.parent_id,
                web_icon: app.web_icon
            };

            if (!app.parent_id) {
                if (app.web_icon_data) {
                    item.web_icon_data = 'data:image/png;base64,' + app.web_icon_data;
                } else if (app.web_icon) {
                    var icon = app.web_icon.split(',');
                    item.web_icon = {
                        foreground: icon[0],
                        background: icon[1],
                        icon_name: icon[2],
                    };
                } else {
                    item.web_icon_data = '/web/static/src/img/icon.png';
                }
            } else {
                item.menu_id = menu_id;
            }

            if (item.action) {
                result.push(item);
            }

            if (app.children && app.children.length) {
                for (var i in app.children) {
                    if (app.children[i].parent_id === false) {
                        menu_id = app.children[i].id;
                    }
                    this._getChildrenList(app.children[i], result, menu_id);
                }
            }
            return result;
        },
        _onMenuitemClick: function (e) {
            e.preventDefault();
            var parent_menu_id = parseInt(e.currentTarget.dataset['parentMenu']);
            var menu_id = e.currentTarget.dataset['menu'];
            if(parent_menu_id){
                menu_id = parseInt(menu_id);
                this._openMenu(_.findWhere(this.all_menu_data, {id: menu_id}).action, parent_menu_id);
            }else {
                return false;
            }
        },
        _openMenu: function (action, menu_id) {
            this.trigger_up('on_menu_action', {
                menu_id: menu_id,
                action_id: action
            });
        },
        _handleMoreItems: function () {
            if (!this.$el.is(":visible")) return;

            this.$o_menu_sections = this.$el.find('.o_menu_sections').removeClass('o_hidden');
            this.$o_menu_brand = this.$el.find('.o_menu_brand').removeClass('o_hidden');
            this.$o_menu_toggle = this.$el.find('.o_menu_toggle');

            if (this.$moreItems) {
                this.$moreItems.find("> ul > *").appendTo(this.$o_menu_sections);
                this.$moreItems.remove();
            }

            if (config.device.size_class < config.device.SIZES.SM) return;

            var width = this.$el.width();
            var menuItemWidth = this.$o_menu_sections.outerWidth(true);
            var othersWidth = this.$o_menu_toggle.outerWidth(true) + this.$o_menu_brand.outerWidth(true) + this.systray_menu.$el.outerWidth(true);

            if (width < menuItemWidth + othersWidth) {
                var $items = this.$o_menu_sections.children();
                var itemsLength = $items.length;
                menuItemWidth += 44;
                do {
                    itemsLength--;
                    menuItemWidth -= $items.eq(itemsLength).outerWidth(true);
                } while (width < menuItemWidth + othersWidth);

                this.$moreItems = $("<li/>", {"class": "o_extra_menu_items"});
                this.$moreItems.append($("<a/>", {
                    href: "#",
                    "class": "dropdown-toggle fa fa-plus",
                    "data-toggle": "dropdown"
                }));
                this.$moreItems.append($("<ul/>", {"class": "dropdown-menu"}).append($items.slice(itemsLength).detach()));
                this.$moreItems.appendTo(this.$o_menu_sections);
            }
        },
    });

    return Menu;
});
